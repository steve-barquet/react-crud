/* eslint-disable react/jsx-no-bind */
// ---Dependencys
import { ChangeEvent, ReactElement, useState } from 'react';
// ---Components
import AddTask from 'Cont/Home/components/AddTask';
import RenderTasks from 'Cont/Home/components/RenderTasks/RenderTasks';

export interface Task {
  label: string;
  edit: boolean;
}
// ------------------------------------------ COMPONENT-----------------------------------------
export default function HomeCont(): ReactElement {
  // --- Const Hooks and States
  const [tasks, setTasks] = useState<Task[]>([]);
  const [currentInput, setCurrentInput] = useState('');
  const [editInput, setEditInput] = useState('');
  // ---- Main Methods
  function handleChange(event: ChangeEvent<HTMLInputElement>) {
    setCurrentInput(event.target.value);
  }
  function handleChangeEdition(event: ChangeEvent<HTMLInputElement>) {
    setEditInput(event.target.value);
  }
  function onAddTask() {
    const newTask = { label: currentInput, edit: false };
    setTasks([...tasks, newTask]);
  }
  function onDeleteTask(index: number) {
    const removed = removeByIndex(index, tasks);
    setTasks(removed);
  }
  function onEdit(index: number) {
    const modified = editTask(index, tasks);
    setTasks(modified);
  }
  function onEditable(index: number) {
    const modified = changeEditFlag(index, tasks);
    setTasks(modified);
  }
  // ---- Auxiliar Methods
  function removeByIndex(index: number, array: Task[]) {
    let newArray: Task[] = [];
    array.forEach((element, i) => {
      if (i !== index) {
        newArray = [...newArray, element];
      }
    });
    return newArray;
  }
  // ---- Auxiliar Methods
  function changeEditFlag(index: number, array: Task[]) {
    let newArray: Task[] = [];
    array.forEach((element, i) => {
      const currentTask = i === index ? { ...element, edit: true } : element;
      newArray = [...newArray, currentTask];
    });
    return newArray;
  }
  function editTask(index: number, array: Task[]) {
    let newArray: Task[] = [];
    array.forEach((element, i) => {
      const currentTask = i === index ? { label: editInput, edit: false } : element;
      newArray = [...newArray, currentTask];
    });
    return newArray;
  }
  // -------------- Render-------------
  return (
    <>
      <h1>To do</h1>
      <AddTask handleChange={handleChange} onAddTask={onAddTask} />
      <hr />
      <RenderTasks
        onDeleteTask={onDeleteTask}
        handleChangeEdition={handleChangeEdition}
        onEdit={onEdit}
        onEditable={onEditable}
        tasks={tasks}
      />
    </>
  );
}
