// ---Dependencys
import { ReactElement, ChangeEvent } from 'react';
// ---Types
import { Task } from 'Cont/Home/HomeCont';
// ------------------------------------------ PROPS-----------------------------------------
interface Props {
  task: Task;
  index: number;
  onDeleteTask: (index: number)=> void;
  onEditable: (index: number)=> void;
  handleChangeEdition: (event: ChangeEvent<HTMLInputElement>) => void;
  onEdit: (index: number) => void;
}
// ------------------------------------------ COMPONENT-----------------------------------------
export default function ListItem(props: Props): ReactElement {
  const {
    task, index, onDeleteTask, onEditable, handleChangeEdition, onEdit
  } = props;
  // -------------- Render-------------
  if (task.edit) {
    return (
      <li>
        <input type="text" onChange={(event) => { handleChangeEdition(event); }} />
        <button type="button" onClick={() => { onEdit(index); }}>Modify</button>
      </li>
    );
  }
  return (
    <li>
      {task.label}
      <button type="button" onClick={() => onDeleteTask(index)}>-</button>
      <button type="button" onClick={() => onEditable(index)}>edit</button>
    </li>
  );
}
