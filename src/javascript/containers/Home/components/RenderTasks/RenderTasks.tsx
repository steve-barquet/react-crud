// ---Dependencys
import { ReactElement, ChangeEvent } from 'react';
// ---Types
import { Task } from 'Cont/Home/HomeCont';
import ListItem from 'Cont/Home/components/RenderTasks/components/ListItem';

// ------------------------------------------ PROPS-----------------------------------------
interface Props {
  tasks: Task[];
  onDeleteTask: (index: number) => void;
  onEditable: (index: number) => void;
  handleChangeEdition: (event: ChangeEvent<HTMLInputElement>) => void;
  onEdit: (index: number) => void;
}
// ------------------------------------------ COMPONENT-----------------------------------------
export default function RenderTasks(props: Props): ReactElement {
  const {
    tasks, onDeleteTask, onEditable, handleChangeEdition, onEdit
  } = props;
  return (
    <ol>
      {tasks.map((element, i) => (
        <ListItem
          task={element}
          index={i}
          onDeleteTask={onDeleteTask}
          onEditable={onEditable}
          handleChangeEdition={handleChangeEdition}
          onEdit={onEdit}
        />
      ))}
    </ol>
  );
}
