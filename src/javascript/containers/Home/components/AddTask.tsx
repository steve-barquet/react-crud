// ---Dependencys
import { ChangeEvent, ReactElement } from 'react';

// ------------------------------------------ PROPS-----------------------------------------
interface Props {
    handleChange: (event: ChangeEvent<HTMLInputElement>) => void;
    onAddTask: () => void;
}
// ------------------------------------------ COMPONENT-----------------------------------------
export default function AddTask({ handleChange, onAddTask }: Props): ReactElement {
  return (
    <p>
      Task:
      &nbsp;
      <input type="text" onChange={handleChange} />
      <button type="button" onClick={onAddTask}>Add +</button>
    </p>
  );
}
